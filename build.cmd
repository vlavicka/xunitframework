@echo off
setlocal enabledelayedexpansion

set SOLUTION=%~dp0\xunitframework.sln
set R_MSBUILD_KEY="\\.\HKLM\SOFTWARE\Microsoft\MSBuild\ToolsVersions\14.0"
set R_MSBUILD_VALUE="MSBuildToolsPath"

FOR /f "tokens=1,2,*" %%A IN ('REG QUERY %R_MSBUILD_KEY% /v %R_MSBUILD_VALUE% 2^>nul') DO SET "MSBUILD_PATH=%%C"
set MSBUILD_BIN="%MSBUILD_PATH%\msbuild.exe"

title BUILD
call %MSBUILD_BIN% %SOLUTION% /target:Build /p:Configuration=Debug;Platform=x64
call %MSBUILD_BIN% %SOLUTION% /target:Build /p:Configuration=Release;Platform=x64
if errorlevel 1 (
    title ERROR
    exit /b 1
) else (
    title TEST
    echo Compilation succeeded ...
    echo ===================================================================================
    echo Running CSharp unit tests ... 
    echo ===================================================================================
    echo.
    %~dp0\build\bin\Debug\CSUnitTestT.exe
    if errorlevel 1 (title FAIL) else (title PASS)
    echo.
    echo.
    echo ===================================================================================
    echo Running CSharp unit tests ... 
    echo ===================================================================================
    echo.
    %~dp0\build\x64\Debug\FSUnitTest.exe
)


:end
endlocal
