namespace UnitTest 
{
    using System;
    using System.Collections;
    using System.Diagnostics;
    using System.Reflection;

    public struct TestCaseResult 
    {
        private int total;
        private int failed;

        public TestCaseResult(int total, int failed) 
        {
            this.total = total;
            this.failed = failed;
        }

        public int Total
        { 
            get { return this.total; }
        }

        public int Failed
        {
            get { return this.failed; }
        }
    }

    [AttributeUsage(AttributeTargets.Method)]
    public class Skip : Attribute {
        public string Reason { get; set; }
        public Skip() {
            Reason = null;
        }
        public Skip(string reason) {
            Reason = reason;
        }
    }

    [AttributeUsage(AttributeTargets.Method)]
    public class Test : Attribute {
        public string Name { get; set; }
        public Test(string name = null) {
            Name = name;
        }
    }

    [Serializable]
    public class UnitTestAssertionError : Exception 
    {
        public UnitTestAssertionError()
        {
        }

        public UnitTestAssertionError(string message) : base(message)
        {
        }

        public UnitTestAssertionError(string message, Exception inner) : base(message, inner)
        {
        }
    }

    public class TestCase 
    {
        private const string TestCaseMethodPrefix = "Test";
        public Properties TCProperties { get; set; }

        public static string FormatTestCaseName(MethodInfo info)
        {
            string name = info.Name;
            if (HasAttribute(info, typeof(Test))) {
                var testName = ((Test) GetAttribute(info, typeof(Test))).Name;
                if (testName != null) {
                    name = testName;
                }
            }
            return name + " (" + info.DeclaringType.FullName + ")";
        }

        // will return total number of tests and errors
        public static TestCaseResult Runner(TestCase instance)
        {
            string errorLog = string.Empty;
            int failed = 0;
            var filtered = GetListOfTestCases(instance);
            var stopwatch = new Stopwatch();

            stopwatch.Start();
            try {
                instance.GetType().GetMethod("SetUpClass").Invoke(null, null);
            } catch (NullReferenceException) { 
            } catch (Exception) {
                failed += 1;
            }

            try {
                foreach (MethodInfo info in filtered) {
                    Console.Write("{0} ... ", FormatTestCaseName(info));
                    if (HasAttribute(info, typeof(Skip))) {
                        var reason = ((Skip)GetAttribute(info, typeof(Skip))).Reason;
                        if (reason == null) {
                            Console.WriteLine("skipped");
                        } else {
                            Console.WriteLine(string.Format("skipped ({0})", reason));
                        }
                    } else {
                        try {
                            instance.SetUp();
                            info.Invoke(instance, null);
                            Console.WriteLine("ok");
                        } catch (TargetInvocationException tie) {
                            Console.WriteLine("fail");
                            if (tie.InnerException is UnitTestAssertionError) {
                                errorLog += string.Format(
                                                "{0}\nFAIL: {1}\n{2}\n{3}\n{4}\n\n",
                                                new string('=', 60),
                                                FormatTestCaseName(info),
                                                new string('-', 60),
                                                tie.InnerException.StackTrace,
                                                tie.InnerException.Message);
                            } else {
                                Console.WriteLine("Unexcpected exception:");
                                Console.WriteLine(tie);
                            }
                            failed += 1;
                        } finally {
                            instance.TearDown();
                        }
                    }
                }
            } finally {
                try {
                    instance.GetType().GetMethod("TearDownClass").Invoke(null, null);
                } catch (NullReferenceException) {
                } catch (Exception) {
                    failed += 1;
                }
                stopwatch.Stop();
            }

            if (failed != 0)
            {
                Console.WriteLine("\n" + errorLog);
            }

            TimeSpan ts = stopwatch.Elapsed;
            Console.WriteLine("------------------------------------------------------------");
            Console.WriteLine("Ran {0} tests in {1:00}:{2:00}.{3:00} sec\n", filtered.Count, ts.Minutes, ts.Seconds, ts.Milliseconds);

            if (failed != 0)
            {
                Console.WriteLine("FAILED (failures={0}, errors=0, skipped=0)", failed);
            }

            return new TestCaseResult(filtered.Count, failed);
        }

        public string RepeatString(int times, char character)
        {
            return new string(character, times);
        }

        public void AssertTrue(bool expr, string message = "'false' is not 'true'") 
        {
            if (!expr) 
            {
                throw new UnitTestAssertionError("AssertionError: " + message);
            }
        }

        public void AssertFalse(bool expr, string message = "'true' is not 'false'") 
        {
            this.AssertTrue(!expr, message);
        }

        public void AssertRaises(Type exc, Delegate fnc, params object[] args) 
        {
            try 
            {
                fnc.DynamicInvoke(args);
            }
            catch (System.Reflection.TargetInvocationException e)
            {
                if (e.InnerException.GetType().Name == exc.Name)
                {
                    return;
                }
                else
                {
                    throw;
                }
            }

            throw new UnitTestAssertionError("AssertionError: exception was not raised");
        }

        public void AssertRaises(Type exc, Action fnc) 
        {
            this.AssertRaises(exc, (Delegate)fnc);
        }

        public void AssertRaises<T>(Type exc, Func<T>fnc) 
        {
            this.AssertRaises(exc, (Delegate)fnc);
        }

        public void AssertEqual<T>(T expected, T result, string message = "") where T : IComparable 
        {
            if (expected.CompareTo(result) != 0) 
            {
                if (message == string.Empty)
                {
                    message = this.FormatFailureMessage(expected, result);
                }

                throw new UnitTestAssertionError("AssertionError: " + message);
            }
        }

        public void AssertEqual<T>(T[] expected, T[] result, string message = "") where T : IComparable 
        {
            this.CheckArraySizes(expected, result);
            if (!this.CheckArrayEquality(expected, result)) 
            {
                if (message == string.Empty)
                {
                    message = this.FormatFailureMessage(expected, result);
                }

                throw new UnitTestAssertionError("AssertionError: " + message);
            }
        }

        public void AssertNotEqual<T>(T expected, T result, string message = "") where T : IComparable 
        {
            if (expected.CompareTo(result) == 0) 
            {
                if (message == string.Empty)
                {
                    message = this.FormatFailureMessage(expected, result);
                }

                throw new UnitTestAssertionError("AssertionError: " + message);
            }
        }

        public void AssertNotEqual<T>(T[] expected, T[] result, string message = "") where T : IComparable 
        {
            this.CheckArraySizes(expected, result);
            if (this.CheckArrayEquality(expected, result)) 
            {
                if (message == string.Empty) 
                { 
                    message = this.FormatFailureMessage(expected, result); 
                }

                throw new UnitTestAssertionError("AssertionError: " + message);
            }
        }

        public virtual void SetUp()
        {
        }

        public static void SetUpClass() {

        }

        public virtual void TearDown()
        {
        }

        public static void TearDownClass() {

        }

        private static ArrayList GetListOfTestCases(TestCase instance)
        {
            var methodInfos = instance.GetType().GetMethods(BindingFlags.Public | BindingFlags.Instance); 
            var filtered = new ArrayList();

            foreach (MethodInfo methodInfo in methodInfos)
            {
                if (methodInfo.Name.StartsWith(TestCaseMethodPrefix, StringComparison.InvariantCulture) || HasAttribute(methodInfo, typeof(Test)))
                {
                    filtered.Add(methodInfo);
                }
            }

            return filtered;
        }

        private string FormatFailureMessage<T>(T expected, T result)
        {
            return string.Format("Expected:\n'{0}'\n-----------\nGot:\n'{1}'\n", expected, result);
        }

        private bool CheckArrayEquality<T>(T[] expected, T[] result) where T : IComparable
        {
            var equal = true;
            for (int i = 0; i < expected.Length; ++i)
            {
                equal &= expected[i].CompareTo(result[i]) == 0;
            }

            return equal;
        }

        private void CheckArraySizes<T>(T[] expected, T[] result)
        {
            if (expected.Length != result.Length)
            {
                throw new UnitTestAssertionError("AssertionError: arrays have different size");
            }
        }

        static bool HasAttribute(MethodInfo info, Type t) {
            foreach (var attr in Attribute.GetCustomAttributes(info)) {
                if (attr.GetType() == t) {
                    return true;
                }
            }
            return false;
        }

        static Attribute GetAttribute(MethodInfo info, Type t) {
            foreach (var attr in Attribute.GetCustomAttributes(info)) {
                if (attr.GetType() == t) {
                    return attr;
                }
            }
            return null;
        }
    }
}
