namespace UnitTest 
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Text.RegularExpressions;
    using System.Globalization;
    using System.Reflection;

    using UnitTest;

    public class TestSuite
    {
        readonly List<TestCase> testcases = new List<TestCase>();
        
        public void RegisterTestCase(TestCase test)
        {
            this.testcases.Add(test);
        }

        public List<TestCase> TestCases
        {
            get { return this.testcases; }
        }

        public int Run(string[] args)
        {
            var failed = 0;
            var total = 0;

            try
            {
                var options = new Options(args);
                var tcToRun = new List<TestCase>();

                if (options.Included.Count != 0)
                {
                    foreach (var name in options.Included)
                    {
                        var tc = testcases.Find((y) => y.GetType().Name.Equals(name));
                        if (tc == null)
                        {
                            throw new Exception(string.Format("Invalid name {0}.", name));
                        }
                        tcToRun.Add(tc);
                    }
                }
                else
                {
                    tcToRun = testcases;
                }

                foreach (TestCase tc in tcToRun)
                {
                    tc.TCProperties = options.TCProperties;
                    var result = TestCase.Runner(tc);
                    failed += result.Failed;
                    total += result.Total;
                }

                Console.WriteLine("================================================================================");
                Console.WriteLine(string.Format("TEST SUITE RESULTS: run {0} test from which {1} has failed.\n", total, failed));
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("Error in test suite execution: {0} -> {1}", ex.GetType().Name, ex.Message));
                return 1;
            }

            return failed != 0 ? 1 : 0;
        }

        public class Options
        {
            public Properties TCProperties { get; private set; }
            public List<string> Included { get; private set; }

            public Options(string[] args)
            {
                TCProperties = new Properties();
                Included = new List<string>();

                var iter = args.GetEnumerator();
                while (iter.MoveNext())
                {
                    switch ((string) iter.Current)
                    {
                    case "-p":
                        iter.MoveNext();
                        TCProperties.Add((string) iter.Current);
                        break;
                    default:
                        Included.Add((string) iter.Current);
                        break;
                    }
                }
            }
        }
    }

    public class Properties
    {
        Dictionary<string, string> properties = new Dictionary<string, string>();
        static readonly Regex PatternProperty = new Regex(@"^([\w]+)=(.*)$");

        public void Add(string value)
        {
            var matched = PatternProperty.Match(value);
            if (matched.Success)
            {
                properties.Add(matched.Groups[1].Value, matched.Groups[2].Value);
            }
            else
            {
                throw new Exception(string.Format("Invalid property definition. Should be '<key>=<value>'. Found '{0}'", value));
            }
        }

        public int Count
        {
            get {
                return properties.Count;
            }
        }

        public string Get(string name)
        {
            return properties[name];
        }

        public int GetAsInt(string name)
        {
            return int.Parse(Get(name));
        }

        public float GetAsFloat(string name)
        {
            return float.Parse(Get(name), CultureInfo.InvariantCulture);
        }

        public bool GetAsBoolean(string name)
        {
            return bool.Parse(Get(name));
        }

    }
}
