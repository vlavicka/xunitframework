namespace UnitTest
{
    using System;

    public class Program
    {
        public static int Main()
        {
            var suite = new TestSuite();
            suite.RegisterTestCase(new TestCaseCompares());
            suite.RegisterTestCase(new TestCaseExceptionsCheck());
            suite.RegisterTestCase(new TestSuiteCommandLineArguments());
            suite.RegisterTestCase(new TestAnnotations());
            return suite.Run(new string[] { "-p", "TEST=test" });
        }
    }

    [Serializable]
    public class MyError : Exception
    {
        public MyError()
        {
        }

        public MyError(string message) : base(message)
        {
        }

        public MyError(string message, Exception inner) : base(message, inner)
        {
        }
    }

    public class TestCaseCompares : TestCase
    {
        int tmp;

        public override void SetUp()
        {
            this.tmp = 10;
        }

        public override void TearDown()
        {
            this.tmp = 0;
        }

        public void TestAssertTrue()
        {
            this.AssertTrue(true, "message");
        }

        public void TestAssertFalse()
        {
            this.AssertFalse(false, "message");
        }

        public void TestAssertEqualStrings()
        {
            this.AssertEqual("TEST", "TEST", "message");
        }

        public void TestSetUpAndTearDown()
        {
            this.AssertEqual(this.tmp, 10);
        }

        public void TestAssertEqualNumbers()
        {
            this.AssertEqual(10, 10, "message");
        }

        public void TestAssertEqualTuple()
        {
            this.AssertEqual(
                new Tuple<string, int>("TEST", 10), 
                new Tuple<string, int>("TEST", 10),
                "message");
        }

        public void TestAssertEqualArray()
        {
            this.AssertEqual(
                new int[] { 1, 2, 3 },
                new int[] { 1, 2, 3 },
                "message");

            this.AssertEqual(
                new double[] { 1.1, 2.2, 3.3 },
                new double[] { 1.1, 2.2, 3.3 },
                "message");
        }

        public void TestAssertNotEqualNumbers()
        {
            this.AssertNotEqual(10, 11, "message");
        }

        public void TestAssertNotEqualArray()
        {
            this.AssertNotEqual(
                new int[] { 1, 2, 3 },
                new int[] { 2, 2, 3 },
                "message");
        }

        public void TestAssertEqualArraySizeDifference()
        {
            this.AssertRaises(
                typeof(UnitTest.UnitTestAssertionError),
                new Action<int[], int[], string>(this.AssertEqual),
                new int[] { 1, 2 },
                new int[] { 1, 4, 3 },
                string.Empty);
        }

        public void TestAssertNotEqualArraySizeDifference()
        {
            this.AssertRaises(
                typeof(UnitTest.UnitTestAssertionError),
                new Action<int[], int[], string>(this.AssertEqual),
                new int[] { 1, 2 },
                new int[] { 1, 2, 3 },
                string.Empty);
        }
    }

    public class TestCaseExceptionsCheck : TestCase
    {
        public void TestAssertRaises()
        {
            this.AssertRaises(typeof(MyError), new Action(this.FncNoParam));
            this.AssertRaises(typeof(MyError), new Action<int>(this.FncParameters), 10);
        }

        public void TestAssertRaisesWithLambda()
        {
            this.AssertRaises(typeof(MyError), () => this.FncNoParam());
            this.AssertRaises(typeof(MyError), () => this.FncParameters(10));
        }

        public void FncNoParam()
        {
            throw new MyError("error");
        }

        public void FncParameters(int i)
        {
            throw new MyError("error");
        }
    }

    public class TestSuiteCommandLineArguments : TestCase
    {
        private TestSuite suite;
        private TestCase tc1, tc2;

        public override void SetUp()
        {
            tc1 = new TestCaseCompares();
            tc2 = new TestCaseExceptionsCheck();

            suite = new TestSuite();
            suite.RegisterTestCase(this.tc1);
            suite.RegisterTestCase(this.tc2);
        }

        public void TestIncludeTests()
        {
            string[] input = { "TestCaseCompares" };
            var result = new TestSuite.Options(input);
            AssertEqual(this.tc1.GetType().Name, result.Included[0]);
        }

        public void TestDefineProperties()
        {
            var result = new TestSuite.Options(new string[] { "-p", "KEY=VALUE" });
            AssertEqual(1, result.TCProperties.Count);
            AssertEqual("VALUE", result.TCProperties.Get("KEY"));
        }

        public void TestDefinePropertyInt()
        {
            var result = new TestSuite.Options(new string[] { "-p", "KEY=10" });
            AssertEqual(1, result.TCProperties.Count);
            AssertEqual(10, result.TCProperties.GetAsInt("KEY"));
        }

        public void TestDefinePropertyFloat()
        {
            var result = new TestSuite.Options(new string[] { "-p", "KEY=10.1" });
            AssertEqual(1, result.TCProperties.Count);
            AssertEqual(10.1f, result.TCProperties.GetAsFloat("KEY"));
        }

        public void TestDefinePropertyBool()
        {
            // TODO: add more tests for Bool value representation: false, 0, 1
            var result = new TestSuite.Options(new string[] { "-p", "BOOL_01=true", "-p", "BOOL_02=false" });
            AssertEqual(2, result.TCProperties.Count);
            AssertTrue(result.TCProperties.GetAsBoolean("BOOL_01"));
            AssertFalse(result.TCProperties.GetAsBoolean("BOOL_02"));
        }

        // TODO: add error case
        public void TestDefinePropertyUsage()
        {
            AssertEqual("test", TCProperties.Get("TEST"));
        }
    }

    public class TestAnnotations : TestCase {
        [Skip]
        public void TestSkipThis() {
            AssertFalse(true);
        }

        [Skip("Skip with reason")]
        public void TestSkipThisWithReason() {
            AssertFalse(true);
        }

        [Test("This is test annotation.")]
        public void ThisIsTestAnnotation() {
            AssertTrue(true);
        }

        [Test]
        public void ThisIsTestAnnotationWithoutName() {
            AssertTrue(true);
        }

        [Skip]
        [Test]
        public void ThisIsTestAnnotationThatIsSkipped() {
            AssertFalse(true);
        }

        public void TestWithoutTestAnnotation() {
            AssertTrue(true);
        }
    }
}
