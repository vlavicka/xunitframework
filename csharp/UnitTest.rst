======================
C# Unit Test framework
======================


C# Unit Test framework is based on xUnit


AssertRaises
============

AssertRaises(Exception, function, argumnets)


Exception
---------
.. example::
   
   typeof(SomeException)



Function
--------

.. without parameters::

   new Action(SomeFunction)


.. with parameters::

   new Action<int>(SomeOtherFunction)



