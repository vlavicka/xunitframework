let csharp_build_basedir = expand('<sfile>:h:p')

function! HandleCommandError(error_info)
    if v:shell_error != 0
        echon "failed"
        echo a:error_info
    else
        echon "ok"
    endif
endfunction

function! CSBuildWindows()
    let CSPROJECT = g:csharp_build_basedir."\\UnitTest.csproj"
    let MSBUILD_BIN = "\"C:\\Program Files (x86)\\MSBuild\\12.0\\Bin\\MSBuild.exe\""

    echo "Compilling succeeded ... "
    let build_result = system(MSBUILD_BIN." ".CSPROJECT." /target:Build /p:Configuration=Debug;Platform=x64")
    call HandleCommandError(build_result)

    echo "Running unit tests ... "
    let test_result = system(g:csharp_build_basedir."\\bin\\x64\\Debug\\UnitTest.exe")
    call HandleCommandError(test_result)
    echo test_result
endfunction

function! CSBuildLinux()
    echo "Not supported"
endfunction

if IsLinux()
    map <F7> :wall<CR>:call CSBuildLinux()")<CR>
else
    map <F7> :wall<CR>:call CSBuildWindows()")<CR>
endif
