namespace xUnit

open System.Text.RegularExpressions

(* base for test framework *)

module TestCase =
    let rec compareLists x y =
        match (x, y) with
        | ([], []) -> true
        | (xth::xtl, yth::ytl) ->
            match xth = yth with
            | true -> compareLists xtl ytl
            | false -> false
        | _ -> false

    let assertEqual expected result =
        match expected.Equals(result) with
        | true -> ()
        | false -> failwith (sprintf "Test failure\nExpected:\n  '%s'\nGot:\n  '%s'" expected result)

    let assertEqualList expected result =
        match (compareLists expected result) with
        | true -> ()
        | false -> failwith (sprintf "Test failure\nExpected:\n  '%s'\nGot:\n  '%s'" (expected.ToString()) (result.ToString()))
            

    let assertTrue value =
        match value with
        | true -> ()
        | false -> failwith "Test failure: False is not True"

    let assertFalse value =
        match value with
        | true -> failwith "Test failure: True is not False"
        | false -> ()

    let assertRaises ex fn =
        let mutable failed = true
        try
            fn ()
        with
            | ex -> failed <- false

        match failed with
            | true -> failwith "TestCase: Exception was not raised!"
            | false -> ()
            

    let runTest title test name = 
        printf "%s (%s) ... " name title
        try
            test ()
            printf "ok\n"
            0
        with
            | exn ->
                printf "fail\n"
                printf "Test '%s' has failed:\n  %s\n" name exn.Message
                1

    let runTestSuite (suite : list<_>) title =
        let mutable failures = 0
        let runner = runTest title
        for (test, name) in suite do
            failures <- failures + (runner test name)
        printf "--------------------------------------------------------------------------------\n"
        printf "Ran %d tests from which %d tests had failed!\n" (suite.Length) failures
        failures

module XmlUtils = 
    let normalizeXml xml = 
        let eol_out = ((new Regex("(\r\n|\n)")).Replace(xml, ""))
        let tag_spaces = ((new Regex(@">[ ]+<")).Replace(eol_out, "><"))
        ((new Regex(@"[ ]{2,}")).Replace(tag_spaces, " "))
