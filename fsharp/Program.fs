﻿// Learn more about F# at http://fsharp.net
// See the 'F# Tutorial' project for more help.

open xUnit

[<EntryPoint>]
let main argv = 
    let tests = [
        (( fun () -> TestCase.assertEqual "a" "a"), "TestCase::AssertEqual");
        (( fun () -> TestCase.assertEqualList [1;2;3] [1;2;3]), "TestCase::AssertEqualList_equal");
        (( fun () -> TestCase.assertEqualList [1;2;3] [1;2;3]), "TestCase::AssertEqualList_equal");
        (( fun () -> TestCase.assertRaises exp (fun () -> TestCase.assertEqualList [1;2;4] [1;2;3])), "TestCase::AssertEqualList_unequal");
        (( fun () -> TestCase.assertRaises exp (fun () -> TestCase.assertEqualList [1;2;3;5] [1;2;3])), "TestCase::AssertEqual_different_length");
        (( fun () -> TestCase.assertTrue true), "TestCase::AssertTrue");
        (( fun () -> TestCase.assertFalse false), "TestCase::AssertFalse");

        (( fun () -> TestCase.assertEqual "<data><content/></data>" (XmlUtils.normalizeXml "<data>\n  <content/>\n</data>")), "XmlUtils::Normalize_unix_eol");
        (( fun () -> TestCase.assertEqual "<data><content/></data>" (XmlUtils.normalizeXml "<data>\r\n  <content/>\r\n</data>")), "XmlUtils::Normalize_dos_eol");
        (( fun () -> TestCase.assertEqual "<data><content attr=\"x\"/></data>" (XmlUtils.normalizeXml "<data>\r\n  <content  attr=\"x\"/>\r\n</data>")), "XmlUtils::Normalize_multiple_space");

        (( fun () -> TestCase.assertRaises exp (fun () -> (failwith "ERROR"))) , "TestCase::AssertRaises_pass");
        (( fun () -> TestCase.assertRaises exp (fun () -> (TestCase.assertRaises exp (fun () -> ())))), "TestCase::AssertRaises_fail");
        ]

    TestCase.runTestSuite tests "XUnitFrameworkTest" |> ignore
    
    
    0 // return an integer exit code
